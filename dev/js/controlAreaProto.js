/**
 * Created by Дмитрий on 10.07.2017.
 */

var setHideble = function (jqArr, callback) {
    //$(jquerySelector).addClass('hideble');

    if(!callback){
        callback = function () {}
    }

    jqArr.forEach(function (element, index, arr) {
        element.addClass('hideble');
        if( index == jqArr.length - 1 ){
            callback.apply(this);
        }
    })
};

var removeHideble = function(jqArr){

    jqArr.forEach(function (element, index, arr) {
        element.removeClass('hideble');
    })

};

var findClass = function (jqueryElem, cssClass) {

    return jqueryElem.attr('class').indexOf(cssClass) == -1 ? false : true;
};

var onCreateMethod = function (event, ui) {

    $('.ui-resizable-handle').mouseover(function (eventObj) {
        $('.devider-icon').addClass('hovered');
    });
    $('.ui-resizable-handle').mouseout(function (eventObj) {
        $('.devider-icon').removeClass('hovered');
    });

    /*$('.overflow-helper').css('height', $('.overflow-helper').find('.content-wrap').outerHeight() + 'px')*/
    $('.main-area .ui-controls-wrap').each(function (event) {
        var height = $(this).find('.ui-controls-cont').outerHeight();
        console.log('ui height = ' + height);

        var hiddenElemsHeight = 0;
        $(this).find('.table table').each(function () {
            if( $(this).css('display') == 'none' ){
                hiddenElemsHeight += $(this).outerHeight();
            }
        });

        $(this).css('height', height + hiddenElemsHeight + 'px');



    })

};

var getPercentWidth = function (ui) {

    var padding = parseInt(ui.element.css('padding-right'));
    var fullWidth = ui.element.closest('.thelp').outerWidth() - padding;

    var percent = ui.size.width / fullWidth * 100;

    return percent;
};

var getWidthByPercent = function (jqueryElem, pcent) {

    var padding = parseInt(jqueryElem.css('padding-right'));
    var fullWidth = jqueryElem.closest('.thelp').outerWidth() - padding;

    var width = pcent/100 * fullWidth;
    console.log('pWidth = ' + width);

    return width

};

var slideArea = function (jqueryElem, width, easing, callback) {
    if( !easing ){
        easing = '500'
    }

    if( !callback ){
        callback = function () {}
    }


    var alsoResize = $(jqueryElem.resizable('option', 'alsoResize'));

    jqueryElem.addClass('inSliding').resizable('disable');
    alsoResize.addClass('inSliding').resizable('disable');

    jqueryElem.animate({
        width : width
    }, easing, function () {
        jqueryElem.removeClass('inSliding');
    });

    alsoResize.animate({
        width : width
    }, easing, function () {
        alsoResize.removeClass('inSliding');

        callback.apply();
    });
};

var onResizeMethod = function (event, ui) {


    if( (ui.size.width < f_settings.mainAreaHideSlideWidth) && !findClass(ui.element, 'inSliding')
        && !findClass(ui.element, 'ui-resizable-disabled') ){

        hideSlideControl(ui);
        $('#head-resize .slide-out-btn').show();

    }else if(ui.size.width >= f_settings.mainAreaHideSlideWidth){

        var alsoResize = $(ui.element.resizable('option', 'alsoResize'));
        var remHidebleArr = [
            ui.element,
            alsoResize
        ];
        removeHideble(remHidebleArr);
        $('#head-resize .slide-out-btn').hide();

        if( getPercentWidth(ui) > f_settings.mainAreaExtendWidth && !findClass(ui.element, 'inSliding') ){
            ui.element.mouseup();
            slideArea(ui.element, getWidthByPercent(ui.element, 100) + 'px');
            //slideArea(alsoResize, '1853px');
            $('#head-resize .slide-in-btn').show();
        }
    }

};

var onStop = function (event, ui) {

};


var addControl = function () {

    $('#head-resize .slide-out-btn').click(function (event) {
        //showControl(event.target.closest('.head-controls'), event.currentTarget);

        slideArea( $('#main-resize'), f_settings.mainAreaHideSlideWidth + 50 + 'px', 500, function () {

            var alsoResize = $( $(event.target.closest('.head-controls')).resizable('option', 'alsoResize'));
            var hidebleElemnAr = [
                $(event.target.closest('.head-controls')),
                alsoResize
            ];

            removeHideble(hidebleElemnAr);

            $(event.target.closest('.head-controls')).resizable('enable');
            alsoResize.resizable('enable');

        } );
        $(this).hide();



    });

    $('#head-resize .slide-in-btn').click(function (event) {


        slideArea( $('#main-resize'), getWidthByPercent( $('#main-resize'), f_settings.mainAreaExtendWidth) + 'px', 500,
            function () {
                $('#main-resize').resizable('enable');
                var alsoResize = $($('#main-resize').resizable('option', 'alsoResize'));
                alsoResize.resizable('enable');
            });
        $(this).hide();

    });

    $('.work-area.ui-hidden').removeClass('ui-hidden');


    $("#main-resize").resizable({
        handles:'e',
        alsoResize: '#head-resize',
        minWidth: '1',
        create: function(event, ui){
            onCreateMethod(event, ui);
        },
        resize: function (event, ui) {
            onResizeMethod(event, ui);
        },
        stop: function (event, ui) {
            onStop(event, ui);
        }
    });
    $('#head-resize').resizable({
        handles:'e',
        alsoResize: '#main-resize',
        minWidth: '1',
        create: function(event, ui){
            onCreateMethod(event, ui);
        },
        resize: function (event, ui) {
            onResizeMethod(event, ui);
        },
        stop: function (event, ui) {
            onStop(event, ui);
        }
    });

};

var hideSlideControl = function (ui) {

    ui.element.mouseup();

    console.log('hiding');
    var mainBlockHeight = $('#main-resize').outerHeight();
    $('#main-resize').css('height', mainBlockHeight);

    var alsoResize = $(ui.element.resizable( "option", "alsoResize" ));

//    setHideble('#main-resize');
   var setHidebleArr = [
        ui.element,
        alsoResize
    ];
    setHideble(setHidebleArr, function () {
        slideArea(ui.element, '1px');
    });


    //$('#main-resize').addClass('hideble');





};

/*var showControl = function (headControlHidedElem, btn) {
    var alsoResize = $($(headControlHidedElem).resizable("option", "alsoResize"));

    $(headControlHidedElem).animate({
        width: f_settings.mainAreaHideSlideWidth + 50 + 'px'
    });
    //$(headControlHidedElem).css('width', f_settings.mainAreaHideSlideWidth + 50 + 'px');
    //alsoResize.css('width', f_settings.mainAreaHideSlideWidth + 50 + 'px');
    alsoResize.animate({
        width: f_settings.mainAreaHideSlideWidth + 50 + 'px'
    }, function () {
        removeHideble([$('#main-resize'), $('#head-resize')]);
    });

    $(btn).hide();
};*/


