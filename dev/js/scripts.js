$(document).ready(function () {

    var windowHeight = $(window).outerHeight(true);
    console.log(windowHeight);

    var headerHeight = $('header').outerHeight(true);
    var workHeadrBlockHeight = $('.head-block').outerHeight(true);
    var emptyAreaHeigth= $('.work-area .empty-area').outerHeight(true);
    var summ = headerHeight + workHeadrBlockHeight + emptyAreaHeigth;

    var lostHeight = windowHeight - summ;

    console.log('header h + work head h = ' + summ);
    console.log('lost height = ' + lostHeight);

    if(lostHeight > 0){
        $('.work-area .empty-area').outerHeight(emptyAreaHeigth + lostHeight);
    }

});

$(document).ready(function () {

    $('.checkbox').click(function () {

        var checkboxInput =  $(this).find('input');
        var checkboxElemGalka = $(this).find('i');

        if(checkboxInput.is(':checked')){
            checkboxInput.removeAttr('checked');
            checkboxElemGalka.hide();
        }else{
            checkboxElemGalka.show();
            checkboxInput.attr('checked', 'checked');
        }

    });


    $('#add-control').click(function () {
        addControl();
    })

});

$(document).ready(function () {
    $('.cat-brunch:not(.attr-no-gr .cat-brunch)').click(function () {
        $(this).children('.cat-brunch').show();
        $(this).addClass('active');
    });

    $('.cat-brunch.listed').click(function () {
        $(this).find('.list-items').show();
    });

    $('.work-area .head-block .main-controls .local-controls .cat-view-mode.control').click(function (e) {
        e.preventDefault();

        var targetSelector =  $(this).attr('activate-target');

        $('.catalog-wrap').hide();
        $('.work-area .head-block .main-controls .local-controls .cat-view-mode').removeClass('active');

        $(this).addClass('active');
        $(targetSelector).show();
    });

    $('.devided-list a').click(function () {
        $(this).closest('.devided-list').find('a').removeClass('active');
        $(this).addClass('active');
    });
});

$(document).ready(function () {
    $('.ui-controls-wrap .func-block .table .header-wrap').click(function () {

        if( $(this).closest('.table').attr('class') == 'table active' ){
            $(this).closest('.table').removeClass('active');
        }else{
            $(this).closest('.table').addClass('active');
        }

    });


});




