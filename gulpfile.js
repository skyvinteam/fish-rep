var gulp = require('gulp');
var rigger = require('gulp-rigger');
var watch = require('gulp-watch');
var bsync = require('browser-sync');


var path = {
    dev: {
        html : 'dev/html/**/*.html',
        css : 'dev/css/**/*.css',
        js : 'dev/js/**/*.js',
        fonts: 'dev/fonts/**/*.*',
        images: 'dev/images/**/*.*'
    }
};

gulp.task('html', function () {
    gulp.src(path.dev.html)
        .pipe(rigger())
        .pipe(gulp.dest('build/html'));
});

gulp.task('css', function () {
    gulp.src(path.dev.css)
        .pipe(rigger())
        .pipe(gulp.dest('build/css'));
});

gulp.task('js', function () {
    gulp.src(path.dev.js)
        .pipe(rigger())
        .pipe(gulp.dest('build/js'));
});

gulp.task('fonts', function () {
    gulp.src(path.dev.fonts)
        .pipe(gulp.dest('build/fonts'));
});

gulp.task('images', function () {
    gulp.src(path.dev.images)
        .pipe(gulp.dest('build/images'));
});

gulp.task('build', [
        'html',
        'css',
        'js',
        'fonts',
        'images',
        'watch'
]);

gulp.task('watch', function () {
    watch([
        path.dev.html,
        'dev/templates'
    ], function(event, cb) {
        gulp.start('html');
    });
    watch([path.dev.css], function(event, cb) {
        gulp.start('css');
    });
    watch([path.dev.js], function(event, cb) {
        gulp.start('js');
    });
    watch([path.dev.fonts], function(event, cb) {
        gulp.start('fonts');
    });
    watch([path.dev.images], function(event, cb) {
        gulp.start('images');
    });
});

/*Настройки туннеля для верстки*/
var markupWebServConf = {
    server: {
        baseDir: 'build'
    },
    ttunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "RusFishCom markup from Skyvin"
};

gulp.task('server', function () {
    bsync(markupWebServConf);
});